//
//  Record.h
//  SecretMic
//
//  Created by zack on 11/4/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <AVFoundation/AVFoundation.h>
#import "Voice_ProcessingViewController.h"
#import "MyClass.h"
@interface Record : NSObject 
<AVAudioPlayerDelegate,AVAudioRecorderDelegate>{

}

-(void)StartRecording;
- (void)StopRecording;

-(void) play;
-(void) stopPlaying;

@property(nonatomic,retain)AVAudioRecorder *audioRecorder;
@property(nonatomic,retain)AVAudioPlayer * audioPlayer;
@end
